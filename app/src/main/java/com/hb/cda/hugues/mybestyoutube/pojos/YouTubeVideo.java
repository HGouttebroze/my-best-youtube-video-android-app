package com.hb.cda.hugues.mybestyoutube.pojos;

public class YouTubeVideo {
// id, titre, description, url, categorie
    private Long id;
    private String titre;
    private String description;
    private String url;
    private String categorie;

    public YouTubeVideo() {
    }

    public YouTubeVideo(Long id, String titre, String description, String url, String categorie) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.url = url;
        this.categorie = categorie;
    }

    public Long getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }
}
